<?php

namespace Drupal\filo;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\filo\Entity\EventInterface;

/**
 * Defines the event date storage.
 */
class EventStorage extends SqlContentEntityStorage implements EventStorageInterface {

  /**
   * Whether the archive status refresh should be skipped.
   *
   * @var bool
   */
  protected $skipRefresh = FALSE;

  /**
   * {@inheritdoc}
   */
  public function loadEventsNeedingArchiveStatusUpdate($limit = NULL) {
    $now = new DrupalDateTime();
    $now->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
    $now_formatted = $now->format(DATETIME_DATETIME_STORAGE_FORMAT);

    // First, find all events, which should be marked as past.
    $query = $this->getQuery()
      ->condition('archive_status', EventInterface::ARCHIVE_STATUS_PAST, '>')
      ->condition('date_range.end_value', $now_formatted, '<');
    if ($limit) {
      $query->range(0, $limit);
    }
    $result = $query->execute();
    $ids = $result;

    if ($limit) {
      // Adjust limit by subtracting the actual result size.
      $limit -= count($ids);
      if ($limit <= 0) {
        // Limit reached, load and return the found entities.
        return $this->loadMultiple($ids);
      }
    }

    // Now, find all future events, which should be marked as ongoing.
    $query = $this->getQuery()
      ->condition('archive_status', EventInterface::ARCHIVE_STATUS_ONGOING, '>')
      ->condition('date_range.value', $now_formatted, '<');
    if (!empty($ids)) {
      $query->condition('event_id', $ids, 'NOT IN');
    }
    if ($limit) {
      $query->range(0, $limit);
    }
    $result = $query->execute();
    $ids = array_merge($ids, $result);
    if (empty($ids)) {
      return [];
    }
    return $this->loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  protected function postLoad(array &$entities) {
    foreach ($entities as $entity) {
      if (!$this->skipRefresh) {
        /** @var \Drupal\filo\Entity\EventInterface $entity */
        $changed = $entity->updateArchiveStatus();
        if ($changed) {
          $entity->save();
        }
      }
    }

    return parent::postLoad($entities);
  }

  /**
   * {@inheritdoc}
   */
  public function loadUnchanged($id) {
    // This method is used by the entity save process, triggering an order
    // refresh would cause a save-within-a-save.
    $this->skipRefresh = TRUE;
    $unchanged_order = parent::loadUnchanged($id);
    $this->skipRefresh = FALSE;
    return $unchanged_order;
  }

}
