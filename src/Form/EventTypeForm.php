<?php

namespace Drupal\filo\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\language\Entity\ContentLanguageSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EventTypeForm extends BundleEntityFormBase {

  /**
   * The event date type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $eventDateTypeStorage;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Creates a new EventTypeForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    $this->eventDateTypeStorage = $entity_type_manager->getStorage('filo_event_date_type');
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\filo\Entity\EventTypeInterface $event_type */
    $event_type = $this->entity;
    $event_date_types = $this->eventDateTypeStorage->loadMultiple();
    $event_date_types = array_map(function ($event_date_type) {
      /** @var \Drupal\filo\Entity\EventDateTypeInterface $event_date_type */
      return $event_date_type->label();
    }, $event_date_types);
    // Create an empty event to get the default status value.
    // @todo Clean up once https://www.drupal.org/node/2318187 is fixed.
    if ($this->operation == 'add') {
      $event = $this->entityTypeManager->getStorage('filo_event')->create(['type' => $event_type->uuid()]);
    }
    else {
      $event = $this->entityTypeManager->getStorage('filo_event')->create(['type' => $event_type->id()]);
    }
    /** @var \Drupal\filo\Entity\EventInterface $event */

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $event_type->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $event_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\filo\Entity\EventType::load',
      ],
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
    ];
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('This text will be displayed on the <em>Add event</em> page.'),
      '#default_value' => $event_type->getDescription(),
    ];
    $form['eventDateType'] = [
      '#type' => 'select',
      '#title' => $this->t('Event date type'),
      '#default_value' => $event_type->getEventDateTypeId(),
      '#options' => $event_date_types,
      '#required' => TRUE,
      '#disabled' => !$event_type->isNew(),
    ];
    $form['event_status'] = [
      '#type' => 'checkbox',
      '#title' => t('Publish new events of this type by default.'),
      '#default_value' => $event->isPublished(),
    ];

    if ($this->moduleHandler->moduleExists('language')) {
      $form['language'] = [
        '#type' => 'details',
        '#title' => $this->t('Language settings'),
        '#group' => 'additional_settings',
      ];
      $form['language']['language_configuration'] = [
        '#type' => 'language_configuration',
        '#entity_information' => [
          'entity_type' => 'filo_event',
          'bundle' => $event_type->id(),
        ],
        '#default_value' => ContentLanguageSettings::loadByEntityTypeBundle('filo_event', $event_type->id()),
      ];
      $form['#submit'][] = 'language_configuration_element_submit';
    }

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = $this->entity->save();
    // Update the default value of the status field.
    $event = $this->entityTypeManager->getStorage('filo_event')->create(['type' => $this->entity->id()]);
    $value = (bool) $form_state->getValue('event_status');
    if ($event->status->value != $value) {
      $fields = $this->entityFieldManager->getFieldDefinitions('filo_event', $this->entity->id());
      $fields['status']->getConfig($this->entity->id())->setDefaultValue($value)->save();
      $this->entityFieldManager->clearCachedFieldDefinitions();
    }

    drupal_set_message($this->t('The event type %label has been successfully saved.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.filo_event_type.collection');
    if ($status == SAVED_NEW) {
      /** @var \Drupal\filo\Entity\EventTypeInterface $this->entity */
      filo_add_body_field($this->entity);
      filo_add_dates_field($this->entity);
    }
  }

}
