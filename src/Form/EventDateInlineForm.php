<?php

namespace Drupal\filo\Form;

use Drupal\inline_entity_form\Form\EntityInlineForm;

/**
 * Defines the inline form for event dates.
 */
class EventDateInlineForm extends EntityInlineForm {

  /**
   * The loaded event date types.
   *
   * @var \Drupal\filo\Entity\EventDateTypeInterface[]
   */
  protected $eventDateTypes;

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeLabels() {
    $labels = [
      'singular' => t('date'),
      'plural' => t('dates'),
    ];
    return $labels;
  }

  /**
   * {@inheritdoc}
   */
  public function getTableFields($bundles) {
    $fields = parent::getTableFields($bundles);
    $fields['label']['label'] = t('Title');
    $fields['date_range'] = [
      'type' => 'field',
      'label' => t('Date'),
      'weight' => 10,
    ];
    $fields['status'] = [
      'type' => 'field',
      'label' => t('Status'),
      'weight' => 100,
      'display_options' => [
        'settings' => [
          'format' => 'custom',
          'format_custom_true' => t('Active'),
          'format_custom_false' => t('Inactive'),
        ],
      ],
    ];

    return $fields;
  }

}
