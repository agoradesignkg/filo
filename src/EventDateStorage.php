<?php

namespace Drupal\filo;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\filo\Entity\EventInterface;

/**
 * Defines the event date storage.
 */
class EventDateStorage extends SqlContentEntityStorage implements EventDateStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadEnabled(EventInterface $event) {
    $ids = [];
    /** @noinspection PhpUndefinedFieldInspection */
    foreach ($event->dates as $date) {
      $ids[$date->target_id] = $date->target_id;
    }
    // Speed up loading by filtering out the IDs of disabled dates.
    $query = $this->getQuery()
      ->condition('status', TRUE)
      ->condition('date_id', $ids, 'IN');
    $result = $query->execute();
    if (empty($result)) {
      return [];
    }
    // Restore the original sort order.
    $result = array_intersect_key($ids, $result);

    return $this->loadMultiple($result);
  }

  /**
   * {@inheritdoc}
   */
  public function getUpcomingDates($limit = 2) {
    $now = new DrupalDateTime();
    $now->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
    $now_formatted = $now->format(DATETIME_DATETIME_STORAGE_FORMAT);

    $query = $this->getQuery()
      ->condition('status', TRUE)
      ->condition('date_range.end_value', $now_formatted, '>=')
      // Order by start date.
      ->sort('date_range.value', 'ASC')
      ->range(0, $limit);
    $result = $query->execute();
    if (empty($result)) {
      return [];
    }
    return $this->loadMultiple($result);
  }

  /**
   * {@inheritdoc}
   */
  public function getDateIdsByMonth($month, $year) {
    $begin = DrupalDateTime::createFromArray(['month' => $month, 'year' => $year, 'day' => 1]);
    $days_of_month = (int)$begin->format('t');
    $end = clone $begin;
    $end->setDate($year, $month, $days_of_month);
    $end->setTime(23, 59, 59);
    $begin_formatted = $begin->format(DATETIME_DATETIME_STORAGE_FORMAT);
    $end_formatted = $end->format(DATETIME_DATETIME_STORAGE_FORMAT);

    $query = $this->getQuery()
      ->condition('status', TRUE)
      ->condition('date_range.end_value', $begin_formatted, '>=')
      ->condition('date_range.value', $end_formatted, '<=')
      ->sort('date_range.value', 'ASC');
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getDatesByMonth($month, $year) {
    $ids = $this->getDateIdsByMonth($month, $year);
    return empty($ids) ? [] : $this->loadMultiple($ids);
  }

  /**
   * @inheritDoc
   */
  public function getDatesByDay(DrupalDateTime $date) {
    $begin = clone $date;
    $begin->setTime(0, 0, 0);
    $end = clone $begin;
    $end->setTime(23, 59, 59);
    $begin_formatted = $begin->format(DATETIME_DATETIME_STORAGE_FORMAT);
    $end_formatted = $end->format(DATETIME_DATETIME_STORAGE_FORMAT);

    $query = $this->getQuery()
      ->condition('status', TRUE)
      ->condition('date_range.end_value', $begin_formatted, '>=')
      ->condition('date_range.value', $end_formatted, '<=')
      ->sort('date_range.value', 'ASC');

    $result = $query->execute();
    if (empty($result)) {
      return [];
    }
    return $this->loadMultiple($result);
  }

}
