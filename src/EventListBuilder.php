<?php

namespace Drupal\filo;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\filo\Entity\EventType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the list builder for events.
 */
class EventListBuilder extends EntityListBuilder {

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * Constructs a new TrailerListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter) {
    parent::__construct($entity_type, $storage);

    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @noinspection PhpParamsInspection */
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = t('Title');
    $header['type'] = t('Type');
    $header['start_date'] = t('Start');
    $header['end_date'] = t('End');
    $header['status'] = t('Status');
    $header['changed'] = t('Changed');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\filo\Entity\EventInterface $entity */
    $event_type = EventType::load($entity->bundle());
    $date_range = $entity->getDateRange();
    $has_dates = !empty($date_range) && !$date_range->isEmpty();

    $row['title']['data'] = $entity->isPublished() ? [
        '#type' => 'link',
        '#title' => $entity->label()
      ] + $entity->toUrl()->toRenderArray() : $entity->label();
    $row['type'] = $event_type->label();
    $row['start_date'] = $has_dates ? $this->dateFormatter->format($date_range->start_date->getTimestamp(), 'short') : '';
    $row['end_date'] = $has_dates ? $this->dateFormatter->format($date_range->end_date->getTimestamp(), 'short') : '';
    $row['status'] = $entity->isPublished() ? $this->t('Published') : $this->t('Unpublished');
    $row['changed'] = $this->dateFormatter->format($entity->getChangedTime(), 'short');

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort('changed', 'DESC');

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}
