<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for events.
 */
interface EventInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  const ARCHIVE_STATUS_PAST = 0;

  const ARCHIVE_STATUS_ONGOING = 1;

  const ARCHIVE_STATUS_FUTURE = 2;

  /**
   * Gets the event title.
   *
   * @return string
   *   The event title
   */
  public function getTitle();

  /**
   * Sets the event title.
   *
   * @param string $title
   *   The event title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Get whether the event is published.
   *
   * Unpublished events are only visible to their authors and administrators.
   *
   * @return bool
   *   TRUE if the event is published, FALSE otherwise.
   */
  public function isPublished();

  /**
   * Sets whether the event is published.
   *
   * @param bool $published
   *   Whether the event is published.
   *
   * @return $this
   */
  public function setPublished($published);

  /**
   * Get whether the event is past.
   *
   * @return bool
   *   TRUE if the event is past, FALSE otherwise.
   */
  public function isPast();

  /**
   * Get whether the event is ongoing.
   *
   * @return bool
   *   TRUE if the event is ongoing, FALSE otherwise.
   */
  public function isOngoing();

  /**
   * Get whether the event is taking place in future.
   *
   * @return bool
   *   TRUE if the event is taking place in future, FALSE otherwise.
   */
  public function isFuture();

  /**
   * Updates the archive_status field, based on comparison with the current time.
   *
   * @return bool
   *   TRUE if the archive status was changed, FALSE otherwise.
   */
  public function updateArchiveStatus();

  /**
   * Gets the event creation timestamp.
   *
   * @return int
   *   The event creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the event creation timestamp.
   *
   * @param int $timestamp
   *   The event creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the event date IDs.
   *
   * @return int[]
   *   The event date IDs.
   */
  public function getDateIds();

  /**
   * Gets the event dates.
   *
   * @return \Drupal\filo\Entity\EventDateInterface[]
   *   The event dates.
   */
  public function getDates();

  /**
   * Gets the next current or future active event date.
   *
   * ATTENTION: There's an implicit assumption, that the referenced dates are
   *   stored in correct ascending order. We don't force the order of the
   *   referenced dates by event date timestamps at all.
   *
   * @return \Drupal\filo\Entity\EventDateInterface|null
   *   The next current (or future) event date.
   */
  public function getNextCurrentDate();

  /**
   * Gets the default event date, which is assumed to be the first active of the
   * referenced event date entities, no matter if the event date is already in
   * the past or not.
   *
   * @return \Drupal\filo\Entity\EventDateInterface|null
   *   The default event date.
   */
  public function getDefaultDate();

  /**
   * Sets the event dates.
   *
   * @param \Drupal\filo\Entity\EventDateInterface[] $dates
   *   The event dates.
   *
   * @return $this
   */
  public function setDates(array $dates);

  /**
   * Gets whether the event has dates.
   *
   * An event must always have at least one date, but a newly initialized
   * (or invalid) event entity might not have any.
   *
   * @return bool
   *   TRUE if the event has dates, FALSE otherwise.
   */
  public function hasDates();

  /**
   * Adds an event date.
   *
   * @param \Drupal\filo\Entity\EventDateInterface $date
   *   The event date.
   *
   * @return $this
   */
  public function addDate(EventDateInterface $date);

  /**
   * Removes an event date.
   *
   * @param \Drupal\filo\Entity\EventDateInterface $date
   *   The event date.
   *
   * @return $this
   */
  public function removeDate(EventDateInterface $date);

  /**
   * Checks whether the event has a given event date.
   *
   * @param \Drupal\filo\Entity\EventDateInterface $date
   *   The event date.
   *
   * @return bool
   *   TRUE if the event date was found, FALSE otherwise.
   */
  public function hasDate(EventDateInterface $date);

  /**
   * Gets the event date range with start and end dates.
   *
   * @return \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem|null
   *   The event date range.
   */
  public function getDateRange();

  /**
   * Sets the event date range.
   *
   * @param \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem $date_range
   *   The event date range.
   *
   * @return $this
   */
  public function setDateRange(DateRangeItem $date_range);

}
