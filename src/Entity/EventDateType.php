<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the event date type entity class.
 *
 * @ConfigEntityType(
 *   id = "filo_event_date_type",
 *   label = @Translation("Event date type"),
 *   label_singular = @Translation("event date type"),
 *   label_plural = @Translation("event date types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event date type",
 *     plural = "@count event date types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\filo\EventDateTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\filo\Form\EventDateTypeForm",
 *       "edit" = "Drupal\filo\Form\EventDateTypeForm",
 *       "delete" = "Drupal\filo\Form\EventDateTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "filo_event_date_type",
 *   admin_permission = "administer filo_event_date_type",
 *   bundle_of = "filo_event_date",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *   },
 *   links = {
 *     "add-form" = "/admin/filo/config/event-date-types/add",
 *     "edit-form" = "/admin/filo/config/event-date-types/{filo_event_date_type}/edit",
 *     "delete-form" = "/admin/filo/config/event-date-types/{filo_event_date_type}/delete",
 *     "collection" =  "/admin/filo/config/event-date-types"
 *   }
 * )
 */
class EventDateType extends ConfigEntityBundleBase implements EventDateTypeInterface {

  /**
   * The event date type ID.
   *
   * @var string
   */
  protected $id;

}
