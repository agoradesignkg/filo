<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the event type entity class.
 *
 * @ConfigEntityType(
 *   id = "filo_event_type",
 *   label = @Translation("Event type"),
 *   label_singular = @Translation("event type"),
 *   label_plural = @Translation("event types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event type",
 *     plural = "@count event types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\filo\EventTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\filo\Form\EventTypeForm",
 *       "edit" = "Drupal\filo\Form\EventTypeForm",
 *       "delete" = "Drupal\filo\Form\EventTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "filo_event_type",
 *   admin_permission = "administer filo_event_type",
 *   bundle_of = "filo_event",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "eventDateType",
 *   },
 *   links = {
 *     "add-form" = "/admin/filo/config/event-types/add",
 *     "edit-form" = "/admin/filo/config/event-types/{filo_event_type}/edit",
 *     "delete-form" = "/admin/filo/config/event-types/{filo_event_type}/delete",
 *     "collection" = "/admin/filo/config/event-types"
 *   }
 * )
 */
class EventType extends ConfigEntityBundleBase implements EventTypeInterface {

  /**
   * The event type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The event type label.
   *
   * @var string
   */
  protected $label;

  /**
   * The event type description.
   *
   * @var string
   */
  protected $description;

  /**
   * The event date type ID.
   *
   * @var string
   */
  protected $eventDateType;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventDateTypeId() {
    return $this->eventDateType;
  }

  /**
   * {@inheritdoc}
   */
  public function setEventDateTypeId($event_date_type_id) {
    $this->eventDateType = $event_date_type_id;
    return $this;
  }

}
