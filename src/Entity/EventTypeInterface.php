<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityDescriptionInterface;

/**
 * Defines the interface for event types.
 */
interface EventTypeInterface extends ConfigEntityInterface, EntityDescriptionInterface {

  /**
   * Gets the event type's matching event date type ID.
   *
   * @return string
   *   The event date type ID.
   */
  public function getEventDateTypeId();

  /**
   * Sets the event type's matching event date type ID.
   *
   * @param string $event_date_type_id
   *   The event date type ID.
   *
   * @return $this
   */
  public function setEventDateTypeId($event_date_type_id);

}
