<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the interface for event date types.
 */
interface EventDateTypeInterface extends ConfigEntityInterface {

}
