<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\user\EntityOwnerInterface;

/**
 * Defines the interface for event dates.
 */
interface EventDateInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the parent event.
   *
   * @return EventInterface|null
   *   The event entity, or null.
   */
  public function getEvent();

  /**
   * Gets the parent event ID.
   *
   * @return int|null
   *   The event ID, or null.
   */
  public function getEventId();

  /**
   * Gets the event title.
   *
   * @return string
   *   The event title
   */
  public function getTitle();

  /**
   * Sets the event title.
   *
   * @param string $title
   *   The event title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Gets whether the event is active.
   *
   * Inactive events are not visible on frontend.
   *
   * @return bool
   *   TRUE if the event is active, FALSE otherwise.
   */
  public function isActive();

  /**
   * Sets whether the event is active.
   *
   * @param bool $active
   *   Whether the event is active.
   *
   * @return $this
   */
  public function setActive($active);

  /**
   * Gets whether the event is already past.
   *
   * @return bool
   *   TRUE if the event is already past, FALSE otherwise.
   */
  public function isPast();

  /**
   * Gets the event creation timestamp.
   *
   * @return int
   *   The event creation timestamp.
   */
  public function getCreatedTime();

  /**
   * Sets the event creation timestamp.
   *
   * @param int $timestamp
   *   The event creation timestamp.
   *
   * @return $this
   */
  public function setCreatedTime($timestamp);

  /**
   * Gets the event date range with start and end dates.
   *
   * @return \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem|null
   *   The event date range.
   */
  public function getDateRange();

  /**
   * Sets the event date range.
   *
   * @param \Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem $date_range
   *   The event date range.
   *
   * @return $this
   */
  public function setDateRange(DateRangeItem $date_range);

  /**
   * Gets the start date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The start date.
   */
  public function getStartDate();

  /**
   * Gets the end date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The end date.
   */
  public function getEndDate();

}
