<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\user\UserInterface;

/**
 * Defines the event date entity class.
 *
 * @ContentEntityType(
 *   id = "filo_event_date",
 *   label = @Translation("Event date"),
 *   label_singular = @Translation("event date"),
 *   label_plural = @Translation("event dates"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event date",
 *     plural = "@count event dates",
 *   ),
 *   bundle_label = @Translation("Event date type"),
 *   handlers = {
 *     "storage" = "Drupal\filo\EventDateStorage",
 *     "access" = "Drupal\filo\EmbeddedEntityAccessControlHandler",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\Core\Entity\ContentEntityForm",
 *     },
 *     "inline_form" = "Drupal\filo\Form\EventDateInlineForm",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler"
 *   },
 *   admin_permission = "administer filo_event_date",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   content_translation_ui_skip = TRUE,
 *   base_table = "filo_event_date",
 *   data_table = "filo_event_date_field_data",
 *   entity_keys = {
 *     "id" = "date_id",
 *     "bundle" = "type",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "label" = "title",
 *     "status" = "status",
 *   },
 *   bundle_entity_type = "filo_event_date_type",
 *   field_ui_base_route = "entity.filo_event_date_type.edit_form",
 * )
 */
class EventDate extends ContentEntityBase implements EventDateInterface {

  use EntityChangedTrait;

  /**
   * @inheritDoc
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * @inheritDoc
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * @inheritDoc
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getEvent() {
    return $this->get('event_id')->entity;
  }

  /**
   * @inheritDoc
   */
  public function getEventId() {
    return $this->get('event_id')->target_id;
  }

  /**
   * @inheritDoc
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * @inheritDoc
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function isActive() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * @inheritDoc
   */
  public function setActive($active) {
    $this->set('status', (bool) $active);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function isPast() {
    $now = new DrupalDateTime();
    $end_date = $this->getEndDate();
    return $end_date < $now;
  }

  /**
   * @inheritDoc
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * @inheritDoc
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getDateRange() {
    return $this->get('date_range');
  }

  /**
   * @inheritDoc
   */
  public function setDateRange(DateRangeItem $date_range) {
    $this->set('date_range', $date_range);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getStartDate() {
    return $this->getDateRange()->start_date;
  }

  /**
   * @inheritDoc
   */
  public function getEndDate() {
    return $this->getDateRange()->end_date;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The event date author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\filo\Entity\EventDate::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    // The event back reference, populated by Event::postSave().
    $fields['event_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event'))
      ->setDescription(t('The parent event.'))
      ->setSetting('target_type', 'filo_event')
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The event date title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['date_range'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Date'))
      ->setDescription(t('The event date.'))
      ->setRequired(TRUE)
      ->setTranslatable(FALSE)
      ->setSettings([
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATETIME,
      ])
      ->setDefaultValue([
        'default_date_type' => 'now',
        'default_date' => 'now',
        'default_end_date_type' => '',
        'default_end_date' => '',
      ])
      ->setDisplayOptions('view', [
        'label' => 'daterange_default',
        'type' => 'string',
        'settings' => [
          'timezone_override' => '',
          'format_type' => 'short',
          'separator' => '-',
        ],
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'daterange_default',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t('Whether the event date is active.'))
      ->setDefaultValue(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 99,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the event date was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the event date was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    $tags = parent::getCacheTagsToInvalidate();
    // Invalidate the event date view builder and event caches.
    return Cache::mergeTags($tags, [
      'filo_event:' . $this->getEventId(),
      'filo_event_date_view',
    ]);
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}