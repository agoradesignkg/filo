<?php

namespace Drupal\filo\Entity;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;
use Drupal\datetime_range\Plugin\Field\FieldType\DateRangeItem;
use Drupal\user\UserInterface;

/**
 * Defines the event class.
 *
 * @ContentEntityType(
 *   id = "filo_event",
 *   label = @Translation("Event"),
 *   label_singular = @Translation("event"),
 *   label_plural = @Translation("events"),
 *   label_count = @PluralTranslation(
 *     singular = "@count event",
 *     plural = "@count events",
 *   ),
 *   bundle_label = @Translation("Event type"),
 *   handlers = {
 *     "storage" = "Drupal\filo\EventStorage",
 *     "access" = "Drupal\entity\EntityAccessControlHandler",
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\filo\EventListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\filo\Form\EventForm",
 *       "add" = "Drupal\filo\Form\EventForm",
 *       "edit" = "Drupal\filo\Form\EventForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *       "delete-multiple" = "Drupal\entity\Routing\DeleteMultipleRouteProvider",
 *     },
 *     "translation" = "Drupal\filo\EventTranslationHandler"
 *   },
 *   admin_permission = "administer filo_event",
 *   permission_granularity = "bundle",
 *   fieldable = TRUE,
 *   translatable = TRUE,
 *   base_table = "filo_event",
 *   data_table = "filo_event_field_data",
 *   entity_keys = {
 *     "id" = "event_id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "langcode" = "langcode",
 *     "uuid" = "uuid",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/event/{filo_event}",
 *     "add-page" = "/event/add",
 *     "add-form" = "/event/add/{filo_event_type}",
 *     "edit-form" = "/event/{filo_event}/edit",
 *     "delete-form" = "/event/{filo_event}/delete",
 *     "delete-multiple-form" = "/admin/filo/events/delete",
 *     "collection" = "/admin/filo/events"
 *   },
 *   bundle_entity_type = "filo_event_type",
 *   field_ui_base_route = "entity.filo_event_type.edit_form",
 * )
 */
class Event extends ContentEntityBase implements EventInterface {

  use EntityChangedTrait;

  /**
   * @inheritDoc
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * @inheritDoc
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * @inheritDoc
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getTitle() {
    return $this->get('title')->value;
  }

  /**
   * @inheritDoc
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * @inheritDoc
   */
  public function setPublished($published) {
    $this->set('status', (bool) $published);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function isPast() {
    return $this->getArchiveStatus() == self::ARCHIVE_STATUS_PAST;
  }

  /**
   * @inheritDoc
   */
  public function isOngoing() {
    return $this->getArchiveStatus() == self::ARCHIVE_STATUS_ONGOING;
  }

  /**
   * @inheritDoc
   */
  public function isFuture() {
    return $this->getArchiveStatus() == self::ARCHIVE_STATUS_FUTURE;
  }

  /**
   * @inheritDoc
   */
  public function updateArchiveStatus() {
    $date_range = $this->getDateRange();
    $old_value = $this->getArchiveStatus();
    if (empty($date_range) || $date_range->isEmpty()) {
      // Treat events without dates as future ones.
      $this->set('archive_status', self::ARCHIVE_STATUS_FUTURE);
      return $old_value != $this->getArchiveStatus();
    }
    $now = new DrupalDateTime();
    $now->setTimezone(new \DateTimezone(DATETIME_STORAGE_TIMEZONE));
    /** @var \Drupal\Core\Datetime\DrupalDateTime $end_date */
    $end_date = $date_range->end_date;
    if ($end_date < $now) {
      $this->set('archive_status', self::ARCHIVE_STATUS_PAST);
    }
    else {
      /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
      $start_date = $date_range->start_date;
      if ($start_date > $now) {
        $this->set('archive_status', self::ARCHIVE_STATUS_FUTURE);
      }
      else {
        $this->set('archive_status', self::ARCHIVE_STATUS_ONGOING);
      }
    }
    return $old_value != $this->getArchiveStatus();
  }

  protected function getArchiveStatus() {
    return $this->get('archive_status')->value;
  }

  /**
   * @inheritDoc
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * @inheritDoc
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function getDateIds() {
    $date_ids = [];
    foreach ($this->get('dates') as $field_item) {
      $date_ids[] = $field_item->target_id;
    }
    return $date_ids;
  }

  /**
   * @inheritDoc
   */
  public function getDates() {
    $dates = $this->get('dates')->referencedEntities();
    return $this->ensureTranslations($dates);
  }

  /**
   * @inheritDoc
   */
  public function getNextCurrentDate() {
    foreach ($this->getDates() as $date) {
      if ($date->isActive() && !$date->isPast()) {
        return $date;
      }
    }
    return NULL;
  }

  /**
   * @inheritDoc
   */
  public function getDefaultDate() {
    foreach ($this->getDates() as $date) {
      if ($date->isActive()) {
        return $date;
      }
    }
    return NULL;
  }

  /**
   * @inheritDoc
   */
  public function setDates(array $dates) {
    $this->set('dates', $dates);
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function hasDates() {
    return !$this->get('dates')->isEmpty();
  }

  /**
   * @inheritDoc
   */
  public function addDate(EventDateInterface $date) {
    if (!$this->hasDate($date)) {
      $this->get('dates')->appendItem($date);
    }
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function removeDate(EventDateInterface $date) {
    $index = $this->getDateIndex($date);
    if ($index !== FALSE) {
      $this->get('dates')->offsetUnset($index);
    }
    return $this;
  }

  /**
   * @inheritDoc
   */
  public function hasDate(EventDateInterface $date) {
    return in_array($date->id(), $this->getDateIds());
  }

  /**
   * @inheritDoc
   */
  public function getDateRange() {
    return $this->get('date_range');
  }

  /**
   * @inheritDoc
   */
  public function setDateRange(DateRangeItem $date_range) {
    $this->set('date_range', $date_range);
    return $this;
  }

  /**
   * Gets the index of the given event date.
   *
   * @param \Drupal\filo\Entity\EventDateInterface $date
   *   The event date.
   *
   * @return int|bool
   *   The index of the given event date, or FALSE if not found.
   */
  protected function getDateIndex(EventDateInterface $date) {
    return array_search($date->id(), $this->getDateIds());
  }

  /**
   * Ensures that the provided entities are in the current entity's language.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface[] $entities
   *   The entities to process.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface[]
   *   The processed entities.
   */
  protected function ensureTranslations(array $entities) {
    $langcode = $this->language()->getId();
    foreach ($entities as $index => $entity) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if ($entity->hasTranslation($langcode)) {
        $entities[$index] = $entity->getTranslation($langcode);
      }
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    // Update date_range property to the min/max value of the child events.
    $this->updateDateRange();

    // Update archive status.
    $this->updateArchiveStatus();
  }

  /**
   * Update date_range property to the min/max value of the child events.
   */
  protected function updateDateRange() {
    /** @var \Drupal\Core\Datetime\DrupalDateTime $min_date */
    $min_date = NULL;
    /** @var \Drupal\Core\Datetime\DrupalDateTime $max_date */
    $max_date = NULL;
    foreach ($this->getDates() as $date) {
      if (!$date->isActive()) {
        continue;
      }

      $start_date = $date->getStartDate();
      if (empty($min_date) || $min_date > $start_date) {
        $min_date = $start_date;
      }

      $end_date = $date->getEndDate();
      if (empty($max_date) || $max_date < $end_date) {
        $max_date = $end_date;
      }
    }
    $date_range = NULL;
    if (!empty($min_date) && !empty($max_date)) {
      $date_range = [
        'value' => $min_date->format(DATETIME_DATETIME_STORAGE_FORMAT),
        'end_value' => $max_date->format(DATETIME_DATETIME_STORAGE_FORMAT),
      ];
    }
    else {
      $date_range = NULL;
    }
    $this->set('date_range', $date_range);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    // Ensure there's a back-reference on each event date.
    foreach ($this->dates as $item) {
      $date = $item->entity;
      if ($date->event_id->isEmpty()) {
        $date->event_id = $this->id();
        $date->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    // Delete the event dates of a deleted event.
    $dates = [];
    foreach ($entities as $entity) {
      if (empty($entity->dates)) {
        continue;
      }
      foreach ($entity->dates as $item) {
        $dates[$item->target_id] = $item->entity;
      }
    }
    /** @var \Drupal\Core\Entity\EntityStorageInterface $date_storage */
    $date_storage = \Drupal::service('entity_type.manager')->getStorage('filo_event_date');
    $date_storage->delete($dates);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The event author.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback('Drupal\filo\Entity\Event::getCurrentUserId')
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The event title.'))
      ->setRequired(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 255,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['path'] = BaseFieldDefinition::create('path')
      ->setLabel(t('URL alias'))
      ->setDescription(t('The event URL alias.'))
      ->setTranslatable(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'path',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setCustomStorage(TRUE);

    $fields['date_range'] = BaseFieldDefinition::create('daterange')
      ->setLabel(t('Date'))
      ->setDescription(t('The event date.'))
      ->setRequired(FALSE)
      ->setTranslatable(FALSE)
      ->setSettings([
        'datetime_type' => DateTimeItem::DATETIME_TYPE_DATETIME,
      ])
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published'))
      ->setDescription(t('Whether the event is published.'))
      ->setDefaultValue(TRUE)
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['archive_status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Archive status'))
      ->setDescription(t('Stores a time based status information, whether the event is past, current or future, for better search and filter performance.'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayOptions('view', [
        'type' => 'hidden',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'hidden',
        'weight' => 99,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time when the event was created.'))
      ->setTranslatable(TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time when the event was last edited.'))
      ->setTranslatable(TRUE);

    return $fields;
  }

  /**
   * Default value callback for 'uid' base field definition.
   *
   * @see ::baseFieldDefinitions()
   *
   * @return array
   *   An array of default values.
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

}