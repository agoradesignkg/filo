<?php

namespace Drupal\filo;

/**
 * Defines the interface for event storage.
 */
interface EventStorageInterface {

  /**
   * Loads the events needing a archive status update.
   *
   * @param int $limit
   *   The maximum number of event dates to load.
   *
   * @return \Drupal\filo\Entity\EventInterface[]
   *   The events.
   */
  public function loadEventsNeedingArchiveStatusUpdate($limit = NULL);

}
