<?php

namespace Drupal\filo;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\filo\Entity\EventInterface;

/**
 * Defines the interface for event date storage.
 */
interface EventDateStorageInterface {

  /**
   * Loads the enabled dates for the given event.
   *
   * @param \Drupal\filo\Entity\EventInterface $event
   *   The event.
   *
   * @return \Drupal\filo\Entity\EventDateInterface[]
   *   The enabled dates.
   */
  public function loadEnabled(EventInterface $event);

  /**
   * Loads the current and upcoming dates, ordered by start date.
   *
   * @param int $limit
   *   The maximum number of event dates to load.
   *
   * @return \Drupal\filo\Entity\EventDateInterface[]
   *   The upcoming dates.
   */
  public function getUpcomingDates($limit = 2);

  /**
   * Fetches the date ids by month and year.
   *
   * @param int $month
   *   The month to look for.
   * @param int $year
   *   The year to look for.
   *
   * @return int[]
   *   The found date ids for the given period.
   */
  public function getDateIdsByMonth($month, $year);

  /**
   * Loads the dates by month and year.
   *
   * @param int $month
   *   The month to look for.
   * @param int $year
   *   The year to look for.
   *
   * @return \Drupal\filo\Entity\EventDateInterface[]
   *   The found dates for the given period.
   */
  public function getDatesByMonth($month, $year);

  /**
   * Loads the dates by month and year.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   The day as date object to look for.
   *
   * @return \Drupal\filo\Entity\EventDateInterface[]
   *   The found dates for the given day.
   */
  public function getDatesByDay(DrupalDateTime $date);

}
